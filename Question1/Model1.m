function Z = Model1(X,Y)
    
    %Some given data
    vmean = 1.65;
    g = 9.8*10^-3;
    Data = [51,198,147,39,144,288,226,13,89,137,98,106,102,100,55,44,129,22,77,82];
    
    %Initial prior set to unity
    Prior = ones;
    
    %starting the for loop for the likelihood
    for i=1:length(Data)
        ri = Data(i);
        Posterior=likelihood(Prior,Y,X,ri,g,vmean);
        Prior = Posterior;
    end 
    Z = Posterior;
end