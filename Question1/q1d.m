clear all;
close all;

%Creating a meshgrid for visualization of the likelihood function 
[X,Y] = meshgrid(50:0.1:250,0.05:0.01:0.5);
Posterior = Model2(X,Y);

figure(1)
surf(X,Y,Posterior) %3D representation of the space
drawnow
xlabel('X (km)')
ylabel('\sigma (km/s)')
zlabel f(X,Y)
colormap summer
shading interp
hold on

%Starting point of Metro-Hastings
px = 210;
py = 0.32;
pz = Model2(px,py); %The z position of function

%How many iterations are being made?
iterations = 100000;
sigmay = 0.00005;
sigmax = 0.005;
t = 0;

%initializing arrays
samples = zeros(iterations,4);
%accepting = zeros(iterations-2000,3);
Position = zeros(iterations,3);
for i= 1:iterations 
    %Proposing a new step 
    NewX = px + (randn(1))*sigmax;
    NewY = py + (randn(1))*sigmay;
    NewZ = Model2(NewX,NewY);
    Position(i,:) = [NewX,NewY,NewZ];
    
    %We now compute the acceptance probability of making the proposed step
    %We can let the Metropolis factor Q = 1 since we are assuming no
    %skewness in the proposed random walk (i.e. we are no biasing between
    %left or right direction)
    acceptance = NewZ/pz; %this is the ration between the new proposed/current position
    
    %This generates a random number and determines whether the step will be taken 
    stepped = randn(1) < acceptance^1000;
    
    %Saving samples into an array
    samples(i,:) = [NewX,NewY,NewZ,acceptance];
    
    %Updating the step 
    if stepped == 1
        
        %Updating the step by accepting them 
        px = NewX;
        py = NewY;
        pz = NewZ;
        
        %approximate burn in iteration
        if i > 2000 %We take 0.1% of the total data 
            t = t + 1;
            
            %Recording the accepted steps
            accepting(t,:) = [px,py,pz];
        end 
        
    else stepped = 0;
        
        %Rejecting the proposed steps and reverting to the old step
        NewX = px;
        NewY = py;
        NewZ = pz;
    end
end 
AverageX = mean(accepting(:,1))
AverageSigma = mean(accepting(:,2))
UncertaintyX = std(accepting(:,1))
UncertaintySigma = std(accepting(:,2))

%Resultant figure superimposed on the meshed function 
figure(1)
plot3(Position(:,1),Position(:,2),Position(:,3),'b.')
drawnow 
hold on
