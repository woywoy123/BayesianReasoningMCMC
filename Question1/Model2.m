function Z = Model2(X,Y)
    vmean = 1.65;
    g = 9.8*10^-3;
    Data = [51,198,147,39,144,288,226,13,89,137,98,106,102,100,55,44,129,22,77,82];
    Prior = ones;
    for i=1:length(Data)
        ri = Data(i);
        Posterior=likelihoodf(Prior,Y,X,ri,g,vmean);
        Prior = Posterior;
    end 
    Z = Posterior;
end
function likelif = likelihoodf(Prior,Sig,X,ri,g,vmean)
    vi = g*(ri+X)/1.9;
    UpperExp = (-(vi - vmean).^2)./(2*Sig.^2);
    BottomFrac = 1.9*sqrt(2*pi)*Sig;
    like = (g.*exp(UpperExp))./BottomFrac;
    likelif= Prior.*like;
end