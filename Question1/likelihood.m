function likeli = likelihood(Prior,Sig,X,ri,g,vmean)

    %This is equation (6)
    vi = sqrt(g.*(ri+X));
    
    %Splitting the function into pieces
    UpperExp = (-(vi - vmean).^2)./(2*Sig.^2);
    BottomFrac = 2*Sig.*sqrt(2*pi*g*(ri+X));
    like = (g.*exp(UpperExp))./BottomFrac; %Complete Function
    
    %Creating the likelihood function output
    likeli= Prior.*like;
end