clear all;
close all;

[X,Y] = meshgrid(50:0.1:250,0.05:0.01:0.5);
Posterior = Givef(X,Y);
surf(X,Y,Posterior)
drawnow
xlabel X
ylabel Sigma
zlabel f(X,Y)
colormap summer
shading interp
hold on

%Starting point of Metro-Hastings
px = 207;
py = 0.35;
pz = Givef(px,py); %The z position of function

%How many iterations are being made?
iterations = 100000;
sigmay = 0.00001;
sigmax = 0.005;
t = 0;
for i= 1:iterations 
    %Proposing a new step 
    NewX = px + (randn(1))*sigmax;
    NewY = py + (randn(1))*sigmay;
    NewZ = Givef(NewX,NewY);

    %We now compute the acceptance probability of making the proposed step
    %We can let the Metropolis factor Q = 1 since we are assuming no
    %skewness in the proposed random walk (i.e. we are no biasing between
    %left or right direction)
    acceptance = NewZ/pz; %this is the ration between the new proposed/current position
    stepped = randn(1) < acceptance; %This generates a random number and determines whether the step will be taken 
    
    %Saving samples into an array
    samples(i,:) = [NewX,NewY,NewZ,acceptance];
    
    %Updating the step 
    if stepped == 1
        px = NewX;
        py = NewY;
        pz = NewZ;
        if i > 2000 %approximate burn in iteration
            t = t + 1;
            accepting(t,:) = [px,py,pz];
            Meanx(t,:) = mean(accepting(:,1));
            Meany(t,:) = mean(accepting(:,2));
            sigmaX(t,:) = std(accepting(:,1));
            sigmaSig(t,:) = std(accepting(:,2));
        end 
        figure(1)
        plot3(NewX,NewY,NewZ,'r.')
        drawnow 
        hold on
    else stepped = 0;
        NewX = px;
        NewY = py;
        NewZ = pz;
    end
end 
