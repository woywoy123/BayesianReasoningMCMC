close all; 
clear all;

vmean = 1.65;
g = 9.8*10^-3;
Data = [51,198,147,39,144,288,226,13,89,137,98,106,102,100,55,44,129,22,77,82];

OddsRatio = 1;
PriA = 1;
PriB = 1;
SumA = 0;
SumB = 0;

StepsX = 0.01;
startX = 50;
X = startX;
EndX = 250;
iterationX = (EndX - startX)/StepsX;

StepsSig = 0.0001;
startSig = 0.05;
Sig = startSig;
EndSig = 0.5;
iterationSig = (EndSig - startSig)/StepsSig;

for y=1:iterationX
    X = X + StepsX;
    Sig = 0;
    for z=1:iterationSig
        Sig = Sig + StepsSig;
        PriA = 1;
        PriB = 1;
        for i=1:length(Data)
            ri = Data(i);
            PriA = A(PriA,Sig,X,ri,g,vmean);
            PriB = B(PriB,Sig,X,ri,g,vmean);
        end
        SumA = SumA + PriA;
        SumB = SumB + PriB;
        Odds = SumB/SumA
    end
end

ProbabilityOfModel2 = 1/(1+1/Odds)

function likeli = A(Prior,Sig,X,ri,g,vmean)
    vi = sqrt(g.*(ri+X));
    UpperExp = (-(vi - vmean).^2)./(2*Sig.^2);
    BottomFrac = 2*Sig.*sqrt(2*pi*g*(ri+X));
    like = (g.*exp(UpperExp))./BottomFrac;
    likeli= Prior.*like;
end

function likelif = B(Prior,Sig,X,ri,g,vmean)
    vi = g*(ri+X)/1.9;
    UpperExp = (-(vi - vmean).^2)./(2*Sig.^2);
    BottomFrac = 1.9*sqrt(2*pi)*Sig;
    like = (g.*exp(UpperExp))./BottomFrac;
    likelif= Prior.*like;
end