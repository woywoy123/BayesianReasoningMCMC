clear all;
close all;

%Some given information from the question
vmean = 1.65;
g = 9.8*10^-3;
Data = [51,198,147,39,144,288,226,13,89,137,98,106,102,100,55,44,129,22,77,82];

%Here I create a meshgrid to display the contour plot
[X,Y] = meshgrid(50:0.1:250,0.05:0.001:0.5);

%Using unity for the inital prior 
Prior = ones;

%Initializing the for loop for iterating through the data
for i=1:length(Data)
    ri = Data(i);
    
    %Calling the function defined under the function section
    Posterior=likelihood(Prior,Y,X,ri,g,vmean);
    
    %Posterior becomes the new prior 
    Prior = Posterior;
end 

%Plotting 
contour(X,Y,Posterior,30)
title('Contour plot of the likelihood function')
xlabel('X (km)')
ylabel('\sigma (km/s)')
grid on 

%Defined function of the likelihood 
function likeli = likelihood(Prior,Sig,X,ri,g,vmean)
    
    %This is equation (6)
    vi = sqrt(g.*(ri+X));
    
    %Splitting the function into pieces
    UpperExp = (-(vi - vmean).^2)./(2*Sig.^2);
    BottomFrac = 2*Sig.*sqrt(2*pi*g*(ri+X));
    like = (g.*exp(UpperExp))./BottomFrac; %Complete function 
    
    %Creating the likelihood function output 
    likeli= Prior.*like;
end

